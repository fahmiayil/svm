# API Documentation

Version Management ini hanya menerima project ['backoffice', 'admin', 'bo_report'] _**(hardcode)**_

---

## Method GET (UI)

**endpoint `/`**

Digunakan untuk management version dari UI dan menampilkan seluruh version pada sebuah project

```
> http://206.189.34.46:4022?prj=xxx
dimana xxx adalah nama project nya
```

**endpoint `/man`**

Digunakan untuk menampilkan readme file ini

---

## Method POST

**endpoint `/`**

Digunakan untuk **incremental** version, dan return version yang baru

```
> http://206.189.34.46:4022
> payload :
> {
    "project": "xxxxx",
    "environtment": "xxxxx",
    "prefix": "xxxxx", (optional jika akan menggunakan sentry)
    "version": "xxxxx" (optional (4 digit version), ex: "0.0.0.0")
  }
```

**endpoint `/get`**

Digunakan **HANYA** untuk get version yang sekarang (tanpa increment)

```
> http://206.189.34.46:4022/get
> payload :
> {
    "project": "xxxxx",
    "environtment": "xxxxx",
  }
```

**endpoint `/purge`**

Digunakan untuk purge version release yang ada pada data project (hapus sentry artifact)

```
> http://206.189.34.46:4022/purge
> payload :
> {
    "project": "xxxxx",
    "environtment": "xxxxx",
    "available_version": [{ //data versi yang ada sekarang (data sudah di kurangi dengan yang di hapus)
      "date": "datetime",
      "version": "yyyyyy",
      "sentry": "ppppp"
    }]
  }
```
