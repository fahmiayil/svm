const fs = require("fs");

const defaultVersion = (env, ver) => {
  return {
    [env]: {
      version: ver,
      date: new Date(),
    },
  };
};

const nextVersion = (currVersion) => {
  const lastNumber = currVersion.split(".");
  lastNumber[3] = +lastNumber[3] + 1;
  return lastNumber.join(".");
};

const appendSentryRelease = (path, env, sentryPrefix) => {
  const projectData = JSON.parse(fs.readFileSync(path));
  if (projectData[env]) {
    const sentryRelease = `${sentryPrefix}_${env.toUpperCase()}@${
      projectData[env].version
    }`;
    // contoh hasil BACKOFFICE_DEVELOPMENT@1.2.3.4

    projectData[env].release = sentryRelease;
    const newRelease = {
      date: projectData[env].date,
      version: projectData[env].version,
      sentry: sentryRelease,
    };
    projectData[env].releases = projectData[env].releases
      ? [newRelease, ...projectData[env].releases]
      : [newRelease];
    fs.writeFileSync(path, JSON.stringify(projectData, null, 2));
    return projectData[env];
  }
};

const checkAndUpdateProject = async (path, env, ver, prefix) => {
  const projectData = JSON.parse(fs.readFileSync(path));
  let validVersion = ver;
  if (projectData[env]) {
    projectData[env].version =
      validVersion !== "0.0.0.0"
        ? validVersion
        : nextVersion(projectData[env]?.version);
    projectData[env].date = new Date();
  } else {
    projectData[env] = {
      version: ver,
      date: new Date(),
    };
  }

  fs.writeFileSync(path, JSON.stringify(projectData, null, 2));
  if (prefix) {
    return appendSentryRelease(path, env, prefix);
  }
  return projectData[env];
};

const createProject = (path, env, ver, prefix) => {
  const preset = defaultVersion(env, ver);
  console.log(preset);
  fs.writeFileSync(path, JSON.stringify(preset, null, 2));
  if (prefix) {
    return appendSentryRelease(path, env, prefix);
  }
  return preset[env];
};

module.exports = { defaultVersion, checkAndUpdateProject, createProject };
