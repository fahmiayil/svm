const express = require("express");
const fs = require("fs");
const {
  checkAndUpdateProject,
  defaultVersion,
  createProject,
} = require("./functions");
const moment = require("moment-timezone");
const { marked } = require("marked");

marked.use({
  mangle: false,
  headerIds: false,
});

const app = express();
const port = 4022;

app.use(express.json());
app.set("view engine", "ejs");

// CONSTANTS
const filePath = "./projects/";
const projects = ["backoffice", "bo_report", "admin", "backoffice-login"];

// SERVER
app.get("/", (req, res) => {
  // cara akses http://localhost:4022/?prj=backoffice
  const { prj } = req.query;
  if (prj) {
    const projectPath = `${filePath}${prj}.json`;
    let prData = JSON.parse(fs.readFileSync(projectPath));
    Object.keys(prData).forEach((key) => {
      prData[key].transformDate = moment(prData[key].date)
        .tz("Asia/Jakarta")
        .format("DD/MM/YYYY HH:mm");
    });
    res.render("index", { data: prData });
  } else {
    res.send("SEND YOUR PROJECT");
  }
});

app.get("/man", (req, res) => {
  fs.readFile("./README.md", "utf-8", (_, result) => {
    res.send(marked(result.toString()));
  });
});

app.post("/get", async (req, res) => {
  // body example
  // {
  //   "project": "backoffice",
  //   "environtment": "development",
  // }
  try {
    const { project, environtment } = req.body;
    const projectPath = `${filePath}${project}.json`;
    const projectData = JSON.parse(fs.readFileSync(projectPath));

    res.send(projectData[environtment]);
  } catch (error) {
    res.send("Project Not Found");
  }
});

app.post("/", async (req, res) => {
  // body example
  // {
  //   "project": "backoffice",
  //   "environtment": "development",
  //   "version": "2.2.2.2"
  //   "prefix": "BackOffice" //for sentry
  // }
  try {
    const { project, environtment, version, prefix } = req.body;
    const projectPath = `${filePath}${project}.json`;
    let currentVersion = version || "0.0.0.0";
    let result = {};

    if (projects.includes(project) && environtment) {
      if (fs.existsSync(projectPath)) {
        // PROJECT EXIST
        result = await checkAndUpdateProject(
          projectPath,
          environtment,
          currentVersion,
          prefix
        );
      } else {
        // PROJECT DOES NOT EXIST, AUTO CREATE PROJECT FILE
        result = createProject(
          projectPath,
          environtment,
          currentVersion,
          prefix
        );
      }
      res.send(result);
    } else {
      res.send("we don't recognize your project");
    }
  } catch (error) {
    console.log(error);
    res.send(`Cannot manage version\n ${error}`);
  }
});

app.post("/purge", async (req, res) => {
  // body example
  // {
  //   "project": "backoffice",
  //   "environtment": "development",
  //   "available_version": [{
  //      "date": "2023-07-05T02:54:55.966Z",
  //      "version": "0.1.37.43",
  //      "sentry": "BackOffice_DEVELOPMENT@0.1.37.43"
  //    }] //data versi yang ada sekarang (data sudah di kurangi dengan yang di hapus)
  // }

  try {
    const { project, environtment, available_version } = req.body;
    const projectPath = `${filePath}${project}.json`;
    const projectData = JSON.parse(fs.readFileSync(projectPath));

    const oldVer = projectData[environtment].releases;
    const newVer = available_version.map((ver) => ver.version);
    projectData[environtment].releases = oldVer.filter((oVer) =>
      newVer.includes(oVer.version)
    );

    fs.writeFileSync(projectPath, JSON.stringify(projectData, null, 2));

    res.send(projectData[environtment]);
  } catch (error) {
    res.send("cannot delete version");
  }
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
